function initAutoCompleteIcao(myname, myvalue)
{
    $(myname).autocomplete(
        {
            source: function (request, response)
            {
                $.ajax(
                    {
                        url: "/lookupicao.jsp",
                        data: {
                            startsWith: request.term,
                        },
                        type: "POST",  // POST transmits in querystring format (key=value&key1=value1) in utf-8
                        dataType: "json",   //return data in json format
                        success: function (data)
                        {
                            var results = [];

                            $.map(data.icaos, function (item)
                            {
                                var itemToAdd =  { label: item.icao};
                                results.push(itemToAdd);
                            });

                            return response(results);
                        }
                    });
            },
            minLength: 1,
            delay: 450,
            selectFirst: true,
            autoFocus: true,
            select: function(event, ui)
            {
                $(myname).val(ui.item.label);
                $(myvalue).val(ui.item.label);
            }
        });
}
