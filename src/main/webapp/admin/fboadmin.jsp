<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import=" java.util.List, net.fseconomy.beans.*,  net.fseconomy.data.*"
%>
<%@ page import="net.fseconomy.util.Converters" %>
<%@ page import="net.fseconomy.util.Formatters" %>
<%@ page import="net.fseconomy.dto.FboLottery" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.time.Duration" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.Date" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    if (!Accounts.needLevel(user, UserBean.LEV_MODERATOR))
    {
%>
<script type="text/javascript">document.location.href="index.jsp"</script>
<%
        return;
    }

    String sHoldId = request.getParameter("hold");
    String sReleaseId = request.getParameter("release");

    if(sHoldId != null && !sHoldId.isEmpty())
    {
        int id = Integer.parseInt(sHoldId);
        Fbos.setFboLottoStatus(id, Fbos.LOTTERYSTATUS_HOLD);
    }
    if(sReleaseId != null && !sReleaseId.isEmpty())
    {
        int id = Integer.parseInt(sReleaseId);
        Fbos.setFboLottoStatus(id, Fbos.LOTTERYSTATUS_READY);
    }

    List<FboBean> fbos;

    String pp = request.getParameter("page");
    String title = "FBOs waiting for next lottery";
    int viewswitch = 1;
    if(pp == null || pp.isEmpty())
    {
        pp = "waiting";
        fbos = Fbos.getFbosInwaiting();
    }
    else
    {
        switch (pp)
        {
            case "waiting":
                viewswitch = 1; //Holding
                title = "FBOs waiting for next lottery";
                fbos = Fbos.getFbosInwaiting();
                break;
            case "lottery":
                viewswitch = 2; //Lottery
                title = "FBOs with Active Lottery";
                fbos = Fbos.getFboLotteryActive();
                break;
            case "teardown":
                viewswitch = 3; //teardown
                title = "FBOs Lottery - Awaiting Teardown";
                fbos = Fbos.getFboLotteryAwaitingTeardown();
                break;
            case "closed":
            default:
                viewswitch = 0; //Closed
                title = "Closed FBOs";
                fbos = Fbos.getFbosClosed();
                break;
        }
    }
%>

<!DOCTYPE html>
<html lang="en">
<head>

    <title>FSEconomy terminal</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

    <link href="../css/Master.css" rel="stylesheet" type="text/css" />
    <link href="../css/tablesorter-style.css" rel="stylesheet" type="text/css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type='text/javascript' src='../scripts/jquery.tablesorter.js'></script>
    <script type='text/javascript' src="../scripts/jquery.tablesorter.widgets.js"></script>

    <script type="text/javascript">
        $(function()
        {
            $.extend($.tablesorter.defaults, {
                widthFixed: false,
                widgets : ['zebra','columns']
            });

            $('.fboTable').tablesorter();

            $(".clickableRow").click(function() {
                window.document.location = $(this).data("url");
            });

            $("#newTemplateButton").click(function() {
                window.document.location = "/admin/templateedit.jsp?newtemplate=1";
            });
        });
    </script>

</head>
<body>

<jsp:include flush="true" page="../top.jsp" />
<jsp:include flush="true" page="../menu.jsp" />

<div id="wrapper">
    <div class="content">
        <a href="/admin/admin.jsp">Return to Admin</a><br><br>
        <a href="/admin/fboadmin.jsp?page=closed">Goto Closed FBOs</a><br>
        <a href="/admin/fboadmin.jsp?page=waiting">Goto FBOs waiting for next lottery</a><br>
        <a href="/admin/fboadmin.jsp?page=lottery">Goto FBOs with Active Lottery</a><br>
        <a href="/admin/fboadmin.jsp?page=teardown">Goto FBOs Lottery - Pending Teardown</a><br>

<%
    if(viewswitch == 0 || viewswitch == 1)
    {
%>
        <table class="fboTable tablesorter-default tablesorter" style="width: auto;">
            <caption>
                <%=title%> (<%=fbos.size()%>)
            </caption>
            <thead>
            <tr>
<%
        if(viewswitch == 1)
        {
%>
                <th>Action</th>
<%
        }
%>
                <th>ICAO</th>
                <th>FBO Name</th>
                <th>Status</th>
                <th class="numeric">Price</th>
                <th class="numeric">Lots</th>
                <th class="numeric">Days Since Close</th>
                <th>Owner</th>
            </tr>
            </thead>
            <tbody>
<%
        for (FboBean fbo : fbos)
        {
            GoodsBean supplies = Goods.getGoods(fbo.getLocation(), fbo.getOwner(), GoodsBean.GOODS_SUPPLIES);
            GoodsBean fuel = Goods.getGoods(fbo.getLocation(), fbo.getOwner(), GoodsBean.GOODS_FUEL100LL);
            GoodsBean jeta = Goods.getGoods(fbo.getLocation(), fbo.getOwner(), GoodsBean.GOODS_FUELJETA);
            GoodsBean buildingmaterials = Goods.getGoods(fbo.getLocation(), fbo.getOwner(), GoodsBean.GOODS_BUILDING_MATERIALS);
            CachedAirportBean ap = Airports.cachedAirports.get(fbo.getLocation());

            long daysClosed = Math.abs((new Date().getTime() - fbo.getInactiveSince().getTime())/86400000);

            boolean fboHoldLotto = Fbos.isFboLottoStatus(fbo.getId(), Fbos.LOTTERYSTATUS_HOLD);
            String action = fboHoldLotto ? "Release" : "Hold";
%>
            <tr>
<%
        if(viewswitch == 1)
        {
%>
                <td><a href="fboadmin.jsp?page=<%=pp%>&<%=fboHoldLotto ? "release=" : "hold="%><%=fbo.getId()%>"><%=action%></a></td>
<%
        }
%>
                <td class="nowrap"><%= Airports.airportLink(ap.getIcao(), ap.getIcao(), response) %></td>
                <td><%= fbo.getName() %></td>
                <td><%= fbo.isActive() ? "Open" : "<span style=\'color: red;\'>Closed</span>" %></td>
                <td class="numeric"><%= fbo.isForSale() ? Formatters.currency.format(fbo.getPrice()) + (fbo.getPriceIncludesGoods() ? " + goods" : "") : "" %></td>
                <td class="numeric"><%= fbo.getFboSize() %></td>
                <td class="numeric"><%= daysClosed %></td>
                <td>
                    <%= Accounts.getAccountNameById(fbo.getOwner())%>
                    <%= " " + (Accounts.isGroup(fbo.getOwner()) ? " (" + Accounts.getAccountNameById(Accounts.accountUltimateGroupOwner(fbo.getOwner())) + ")" : "") %>
                </td>
            </tr>
<%
        }
%>
            </tbody>
        </table>
<%
    }
    else if( viewswitch == 2) //lotteries
    {
%>
        <table class="fboTable tablesorter-default tablesorter" style="width: auto;">
            <caption>
                <%=title%> (<%=fbos.size()%>)
            </caption>
            <thead>
            <tr>
                <th>ICAO</th>
                <th class="numeric">Lots</th>
                <th class="normal">Start Date</th>
                <th class="normal">End Date</th>
                <th class="numeric">Ticket Amt</th>
                <th class="numeric">Amount Due</th>
                <th class="numeric">Tickets Sold</th>
                <th>Names</th>
            </tr>
            </thead>
            <tbody>
<%
        for (FboBean fbo : fbos)
        {
            CachedAirportBean ap = Airports.cachedAirports.get(fbo.getLocation());
            FboLottery fl = Fbos.getFboLottery(fbo.getId());
            String names = Fbos.getFboTicketHolders(fl.id);
            if(names == null)
                names = "";
%>
            <tr>
                <td class="nowrap"><%= Airports.airportLink(ap.getIcao(), ap.getIcao(), response) %></td>
                <td class="numeric"><%= fbo.getFboSize() %></td>
                <td class="normal"><%= Formatters.datemmddyy.format(fl.start) %></td>
                <td class="normal"><%= Formatters.datemmddyy.format(fl.end) %></td>
                <td class="numeric"><%= fl.ticketAmount %></td>
                <td class="numeric"><%= fl.amountDue %></td>
                <td class="numeric"><%= fl.ticketsSold %></td>
                <td><%=names%></td>
            </tr>
<%
        }
%>
            </tbody>
        </table>
<%
    }
    else // Teardowns
    {
%>
        <table style="width: 150px;">
            <caption>
                <%=title%> (<%=fbos.size()%>)
            </caption>
        </table>

        <table class="fboTable tablesorter-default tablesorter" style="width: auto;">
            <thead>
            <tr>
                <th class="normal">ICAO</th>
                <th class="normal">Teardown Date</th>
            </tr>
            </thead>
            <tbody>
<%
        for (FboBean fbo : fbos)
        {
            CachedAirportBean ap = Airports.cachedAirports.get(fbo.getLocation());
            FboLottery fl = Fbos.getFboLottery(fbo.getId(), Fbos.LOTTERYSTATUS_TEARDOWN);
%>
    <tr>
        <td class="nowrap"><%= Airports.airportLink(ap.getIcao(), ap.getIcao(), response) %></td>
        <td class="nowrap"><%= Formatters.datemmddyy.format(fl.closed) %></td>
    </tr>
<%
        }
%>
            </tbody>
        </table>
<%
    }
%>
    </div>
</div>
<%= Calendar.getInstance().getTime() %>
</body>
</html>
