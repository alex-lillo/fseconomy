<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.beans.UserBean, net.fseconomy.data.*"
%>
<%@ page import="net.fseconomy.beans.AnnouncementBean" %>
<%@ page import="java.util.List" %>
<%@ page import="net.fseconomy.util.Formatters" %>
<%@ page import="net.fseconomy.util.Helpers" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    if(!Accounts.needLevel(user, UserBean.LEV_MODERATOR))
    {
%>
<script type="text/javascript">document.location.href="../index.jsp"</script>
<%
        return;
    }

    List<AnnouncementBean> announcements = Announcements.getAnnouncements();
%>

<!DOCTYPE html>
<html lang="en">
<head>

    <title>FSEconomy terminal</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

    <meta http-equiv="refresh" content="60" />

    <link href="../css/Master.css" rel="stylesheet" type="text/css" />
    <link href="../css/tablesorter-style.css" rel="stylesheet" type="text/css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type='text/javascript' src='../scripts/jquery.tablesorter.js'></script>
    <script type='text/javascript' src="../scripts/jquery.tablesorter.widgets.js"></script>

    <script type="text/javascript">
        $(function()
        {
            $.extend($.tablesorter.defaults, {
                widthFixed: false,
                widgets : ['zebra','columns']
            });

            $('.fboTable').tablesorter();

            $(".clickableRow").click(function() {
                window.document.location = $(this).data("url");
            });

            $("#newTemplateButton").click(function() {
                window.document.location = "/admin/templateedit.jsp?newtemplate=1";
            });
        });

        function editAnnouncement(id)
        {
           window.location.href = "/admin/announcementedit.jsp?id=" + id;
        }
    </script>

</head>
<body>

<jsp:include flush="true" page="../top.jsp" />
<jsp:include flush="true" page="../menu.jsp" />

<div id="wrapper">
    <div class="content">
        <h2>Announcements</h2>
        <h5>Click row to edit</h5>
        <table class="fboTable tablesorter-default tablesorter" style="width: auto;">
            <caption>
                Announcements (<%=announcements.size()%>)
                <a href="announcementedit.jsp?action=new">Add New</a>
            </caption>
            <thead>
            <tr>
                <th class="numeric">Active</th>
                <th class="normal">Created</th>
                <th class="normal">Updated</th>
                <th class="normal">Post Date</th>
                <th class="normal">Title</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
<%
        for (AnnouncementBean a : announcements)
        {
%>
            <tr class='clickableRow' data-url="/admin/announcementedit.jsp?id=<%= a.getId() %>">
                <td><%=a.getActive() ? "Yes" : "No"%></td>
                <td><%= Formatters.datemmddyy.format(a.getCreated())%></td>
                <td><%= a.getUpdated() != null ? Formatters.datemmddyy.format(a.getUpdated()) : ""%></td>
                <td><%= a.getPosted() != null ? Formatters.datemmddyy.format(a.getPosted()) : ""%></td>
                <td><%=a.getTitle().length() > 80 ? Helpers.truncate(a.getTitle(), 77) + "..." : a.getTitle()%></td>
                <td><a href="/admin/announcementedit.jsp?id=<%= a.getId() %>&action=delete">delete</a></td>
            </tr>
<%
        }
%>
            </tbody>
        </table>

    </div>
</div>
</body>
</html>
