<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="java.util.List, net.fseconomy.beans.*, net.fseconomy.data.*, net.fseconomy.util.*"
%>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session"/>

<%
    if (!user.isLoggedIn())
    {
%>
<script type="text/javascript">document.location.href = "/index.jsp"</script>
<%
        return;
    }

    boolean isDest = false;
    boolean isFBO;
    double latd = 0;
    double lond = 0;

    String icao = request.getParameter("icao");
    CachedAirportBean airportd = Airports.cachedAirports.get(icao);
    CachedAirportBean airportl = Airports.cachedAirports.get(icao);
    List<AssignmentBean> assignments;
    List<FboBean> fboList;
    AssignmentBean assignment;
    GoodsBean fuelleft;

    double latl = airportl.getLatLon().lat;
    double lonl = airportl.getLatLon().lon;

    String icaod = request.getParameter("icaod");
    String type;
    String[] jobs;
    String image;

    double fuelprice;

    if (icaod != null)
    {
        airportd = Airports.cachedAirports.get(icaod);
        latd = airportd.getLatLon().lat;
        lond = airportd.getLatLon().lon;
        isDest = true;
    }
%>

<!DOCTYPE html>
<html lang="en">
<head>

    <title>FSEconomy Google Map</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="map/scripts/map-prototypes.js"></script>

    <jsp:include flush="true" page="gmapapikey.jsp"></jsp:include>

    <script type="text/javascript">
        //SET BEARING - Function by Mike Williams

    </script>

    <script type="text/javascript">
        function initMap() {
            addMapPrototypes();

            // ==================================================
            // *** NO LONGER WORKS IN V3 ***
            //
            // A function to create a tabbed marker and set up the event window
            // This version accepts a variable number of tabs, passed in the arrays htmls[] and labels[]
            function createTabbedMarker(point, htmls, labels) {
                var marker = new google.maps.Marker(point, icondest);
                GEvent.addListener(marker, "click",
                        function () {
                            // adjust the width so that the info window is large enough for this many tabs
                            if (htmls.length > 2) {
                                htmls[0] = '<div style="width:' + htmls.length * 88 + 'px">' + htmls[0] + '</div>';
                            }
                            var tabs = [];
                            for (var i = 0; i < htmls.length; i++) {
                                tabs.push(new google.maps.InfoWindowTab(labels[i], htmls[i]));
                            }
                            marker.openInfoWindowTabsHtml(tabs);
                        }
                );

                return marker;
            }

            /**
             * Returns the zoom level at which the given rectangular region fits in the map view.
             * The zoom level is computed for the currently selected map type.
             * @param {google.maps.Map} map
             * @param {google.maps.LatLngBounds} bounds
             * @return {Number} zoom level
             **/
            function getZoomByBounds(map, bounds) {
                var MAX_ZOOM = 15;
                var MIN_ZOOM = 0;

                var ne = map.getProjection().fromLatLngToPoint(bounds.getNorthEast());
                var sw = map.getProjection().fromLatLngToPoint(bounds.getSouthWest());

                var worldCoordWidth = Math.abs(ne.x - sw.x);
                var worldCoordHeight = Math.abs(ne.y - sw.y);

                //Fit padding in pixels
                var FIT_PAD = 40;
                var mapDiv = document.getElementById('map');
                var width = mapDiv.firstElementChild.clientWidth;
                var height = mapDiv.firstElementChild.clientHeight;
                for (var zoom = MAX_ZOOM; zoom >= MIN_ZOOM; --zoom) {
                    if (worldCoordWidth * (1 << zoom) + 2 * FIT_PAD < width &&
                            worldCoordHeight * (1 << zoom) + 2 * FIT_PAD < height)
                        return zoom;
                }
                return 0;
            }

            function projection_changed() {
                map.setZoom(getZoomByBounds(map, bounds) - 1);
            }

//-----------------------------------------------------------------------------------------------------
            var iconloc = new google.maps.MarkerImage();
            iconloc.image = "img/iconac.png";
            iconloc.shadow = "img/iconshadow.png";
            iconloc.iconSize = new google.maps.Size(12, 20);
            iconloc.shadowSize = new google.maps.Size(20, 20);
            iconloc.iconAnchor = new google.maps.Point(6, 20);
            iconloc.infoWindowAnchor = new google.maps.Point(5, 1);

            var pointloc = new google.maps.LatLng(<%=latl%>, <%=lonl%>);
            var pointdest = new google.maps.LatLng(<%=latd%>, <%=lond%>);

            // get bounding box
            var bounds = new google.maps.LatLngBounds(pointloc, pointloc);
            if (<%=isDest%>) {
                bounds.extend(pointdest);
            }

            // get center point of bounds
            var clat = (bounds.getNorthEast().lat() + bounds.getSouthWest().lat()) / 2;
            var clng = (bounds.getNorthEast().lng() + bounds.getSouthWest().lng()) / 2;
            var mapCenter = new google.maps.LatLng(clat, clng);

            // The map, centered at Uluru
            var map = new google.maps.Map(document.getElementById('map'),
                    {
                        zoom: 12,
                        center: {lat: clat, lng: clng},
                        mapTypeId: google.maps.MapTypeId.SATELLITE,
                        mapTypeControl: true,
                        zoomControl: true,
                        scaleControl: false,
                        streetViewControl: false,
                        fullscreenControl: false
                    });

            google.maps.event.addListenerOnce(map, 'projection_changed', projection_changed);
//----------------------------------------------------------------------------------------------------
            var marker = new google.maps.Marker({
                position: pointloc,
                map: map,
                icon: {
                    url: 'img/iconac.png',
                    size: new google.maps.Size(12, 20)
                }
            });

            var content = "";

            //setup default infoWindow
            var infoWindow = new google.maps.InfoWindow({
                content: "<font face='Verdana' size='1'>No content set.</font>"
            });

            google.maps.event.addListener(infoWindow, 'domready', function() {
                $("#tabs").tabs();
            });


            function createTabName(title, tabNumber)
            {
                return '<li><a href="#tab_'+tabNumber+'"><span>'+title+'</span></a></li>';
            }

            function createTabContent(content, tabNumber)
            {
                return '<div id="tab_'+tabNumber+'"><p>'+content+'</p></div>';
            }

            contentTemplate = [
                '<div id="tabs">',
                    '<ul>',
                        '~TABNAMES~',
                    '</ul>',
                    '~TABCONTENT~',
                '</div>'
            ];

            marker.addListener('click', function () {
                var c = contentTemplate.slice(0); //deep copy for strings
                c[2] = createTabName('Airport', 1);
                c[4] = createTabContent('<%=airportl.getIcao()%><br><%=airportl.getTitle()%>', 1);
                var out = c.join('');
                infoWindow.setContent(out);
                infoWindow.open(map, marker);
            });

            var markerDest;
            if (<%=isDest%>) {
                //SET DISTANCE & BEARING
                var degrees = Math.round(pointloc.bearing(pointdest));
                var msg = "Trip: " + '<font color="#0080C0"><%=icao%></font>' + " to " + '<font color="#0080C0"><%=icaod%></font>' + "&nbsp;&nbsp;&nbsp;&nbsp;Distance: " + "<span style=\"color: #0080C0; \">" + Math.round(pointloc.distanceFrom(pointdest)) + " NM </font>&nbsp;&nbsp;&nbsp;&nbsp;Bearing: " + "<span style=\"color: #0080C0; \">" + degrees + "&#186;</font>";
                document.getElementById("mypoint").innerHTML = msg;


                markerDest = new google.maps.Marker({
                    position: pointdest,
                    map: map,
                    icon: {
                        url: 'img/icondest.png',
                        size: new google.maps.Size(12, 20)
                    }
                });

                var apContent = "<font face='Verdana' size='1'><%=airportd.getIcao()%><br><%=airportd.getTitle()%></font>";
                var jobContent = "";
                var fboContent = "";

                var tabs = createTabName("Airport", 1);
                var tabsContent = createTabContent(apContent, 1);

<%
        assignments = Assignments.getAssignments(icaod, -1, -1, -1, -1);
        CachedAirportBean destination;

        if (assignments.size() != 0)
        {
%>
                jobContent += "<table>";
                //start header
                jobContent += "<tr>";
                jobContent += "<td>Dir</td>";
                jobContent += "<td>Dist</td>";
                jobContent += "<td>ICAO</td>";
                jobContent += "<td>Pay</td>";
                jobContent += "<td>Name</td>";
                jobContent += "<td>Type</td>";
                jobContent += "<td>Expires</td>";
                jobContent += "</tr>";
                //end header
<%
                for (AssignmentBean bean : assignments)
                {
                    assignment = bean;
                    destination = assignment.getDestinationAirport();
                    image = "<img src='img/set2_"+ assignment.getBearingImage() + ".gif'>";
                    type = assignment.getType() == AssignmentBean.TYPE_ALLIN ? "A" : "T";
                    String row =
                        "<td>" + image + "</td>" +
                        "<td>" + assignment.getDistance() + " nm</td>" +
                        "<td>" + destination.getIcao() + "</td>" +
                        "<td>" + assignment.calcPay() + "</td>" +
                        "<td>" + Converters.escapeJavaScript(assignment.getSCargo()) + "</td>" +
                        "<td>" + type  + "</td>" +
                        "<td>" + assignment.getSExpires() + "</td>";
%>
                    jobContent += "<tr>";
                    jobContent += "<%=row%>";
                    jobContent += "</tr>";
<%
                }
%>
                //start content

                //end content
                jobContent += "</table>";

                tabs += createTabName("Jobs", 2);
                tabsContent += createTabContent(jobContent, 2);
<%
        }

        fboList = Fbos.getFboByLocation(icaod);
        fuelprice = airportd.getPrice100ll();
        String fuel = Formatters.currency.format(fuelprice);
        isFBO = false;

        if(fboList.size() != 0 || airportd.has100ll())
        {
%>
                fboContent += "<table>";
                //start header
                fboContent += "<tr>";
                fboContent += "<td>Name</td>";
                fboContent += "<td>100LL Price</td>";
                fboContent += "<td>Gals Avail</td>";
                fboContent += "<td>Repair Shop</td>";
                fboContent += "</tr>";
                //end header
<%
            isFBO = true;
            String row;

            for (FboBean fbo : fboList)
            {
                String temp  ;
                fuelleft = Goods.getGoods(fbo.getLocation(), fbo.getOwner(), GoodsBean.GOODS_FUEL100LL);
                int fuelgallons = 0;
                if (fuelleft != null)
                    fuelgallons = (int)Math.floor(fuelleft.getAmount()/Constants.GALLONS_TO_KG);

//                temp = Converters.escapeJavaScript(fbo.getName())+" |  Fuel: " + Formatters.currency.format(fbo.getFuel100LL())+" | "+ fuelgallons + " gals";
                String repairs = (fbo.getServices() & FboBean.FBO_REPAIRSHOP) > 0 ? "Yes" : "No";

                row =
                    "<td>" + Converters.escapeJavaScript(fbo.getName()) + "</td>" +
                    "<td>" + Formatters.currency.format(fbo.getFuel100LL()) + "</td>" +
                    "<td>" + fuelgallons + "</td>" +
                    "<td>" + repairs + "</td>";
%>
                fboContent += "<tr>";
                fboContent += "<%=row%>";
                fboContent += "</tr>";
<%
            }

            if (airportd.has100ll())
            {
                String repairs = airportd.getSize() >= AircraftMaintenanceBean.REPAIR_AVAILABLE_AIRPORT_SIZE ? "Yes" : "No";
                row =
                    "<td>Local</td>" +
                    "<td>" + fuel + "</td>" +
                    "<td>Unlimited</td>" +
                    "<td>" + repairs + "</td>";
%>
                fboContent += "<tr>";
                fboContent += "<%=row%>";
                fboContent += "</tr>";
<%
            }

%>
                //start content

                //end content
                fboContent += "</table>";

                tabs += createTabName("FBOs", 3);
                tabsContent += createTabContent(fboContent, 3)
<%
        }
%>
            }

            markerDest.addListener('click', function () {
                var tabT = contentTemplate.slice(0); //deep copy for strings
                tabT[2] = tabs;
                tabT[4] = tabsContent;
                var out = tabT.join('');
                infoWindow.setContent(out);
                infoWindow.open(map, markerDest);
            });
        }
    </script>

</head>
<body onload="initMap()" text="#000080" bgcolor="#FFFFFF" background="">

<table border="0" cellpadding="0" style="border-collapse: collapse" width="720" id="table6">
    <tr>
        <td>
            <div id="map" style="width: 600px; height: 500px"></div>
        </td>
    </tr>
    <!-- Div to hold Lat-Lon when I'm using the function -->
    <tr>
        <td>
            <div id="mypoint" style="font-family:Arial, Helvetica, sans-serif;font-size:14pt;line-height:1.1em"></div>
        </td>
    </tr>
</table>

</body>
</html>
